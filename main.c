#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>

#include "game.h"

int s_x=10;
int s_y=10;
int s_sign=1;

void initialize ()
{
	initscr();
	noecho (); /* silent on input */
	curs_set(0); /* cursor invisible */
	keypad ( stdscr, TRUE ); /* enable arrow keys etc. */
	start_color();
	init_pair ( 1, COLOR_YELLOW, COLOR_BLUE );
	init_pair ( 2, COLOR_BLUE, COLOR_YELLOW );
	init_pair ( 3, COLOR_RED, COLOR_BLUE );
	attron( COLOR_PAIR(1) );
	getmaxyx(stdscr,HEIGHT,WIDTH);
}
void create_game_window ()
{
	/* create active game window */
	G_WIDTH=WIDTH-10;
	G_HEIGHT=HEIGHT-4;
	status = STATUS_OK;
	g_win = newwin ( G_HEIGHT, G_WIDTH, 2, 2);
	wattron ( g_win, COLOR_PAIR(2) );
}
void init_data ()
{
	m_first	= 0;
	m_last	= 0;
	s_x	= 10;
	s_y	= 10;
	s_sign	= 1;
	t_x     = G_WIDTH;
	t_y     = 2;
	t_sign  = -1;
	score   = 0;
}
void clean()
{
	attroff ( COLOR_PAIR(1) );
       	endwin ();
}
/*
-------------------------
There are two update mechanisms running in parallel.
One refreshes the screen after a timer expires. This is useful
for refreshing transport ship and missile positions.
Another refreshes the screen on a keypress. This is useful
for refreshing the ship, and for firing missiles.
--------------------------
*/
/* updates screen after a timer expires */
void update(int signum)
{
	wclear ( g_win );
	m_draw ();
	t_update ();
	t_draw ();
	wrefresh ( g_win );
}
void set_timer()
{
	struct sigaction new_action;
	struct itimerval i_timer;

	new_action.sa_handler = update;
	sigemptyset(&new_action.sa_mask);
	new_action.sa_flags = 0;

	sigaction(SIGALRM, &new_action, NULL);

	i_timer.it_value.tv_sec = INTERVAL/1000;
	i_timer.it_value.tv_usec = INTERVAL % 1000000;
	i_timer.it_interval.tv_sec = INTERVAL/1000;
	i_timer.it_interval.tv_usec = INTERVAL % 1000000;
	setitimer(ITIMER_REAL, &i_timer, NULL);
}
int main()
{
	int c;
	int epoch=1;

	initialize ();
	create_game_window ();
	init_data ();
	set_timer();
	box(stdscr,'|','-');
	refresh ();
	while(1){ 
		c = getch ( );
		epoch = (epoch+1) % 100;
		if ( !epoch ){
			t_spawn();
		}
		werase ( g_win ); 
		s_process ( c, &s_y, &s_x, &s_sign );
		if ( status == STATUS_EXIT ){
			clean ();
			return 0;
		}
		s_bound ( &s_x, &s_y );
		s_draw ( s_x, s_y, s_sign );
		m_draw ( );
		t_update ();
		t_draw ();
		wrefresh( g_win );
	}
	return 0;
}
