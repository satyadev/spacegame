#include "game.h"
#include <ncurses.h>
#include <limits.h>

int current_weapon;
int direction;

void busy_wait()
{
	int i;
	int j;

	for ( i=0; i<1000; i++ ){
		for ( j=0; j<10; j++ ){
			;
		}
	}
}

void fire( int current_weapon, int direction )
{
	m_last = (m_last + 1)%10;
	missiles[m_last].x = s_x;
	missiles[m_last].y = s_y;
	missiles[m_last].direction=s_sign;
	missiles[m_last].potency=1;
}

int m_outside ( int i )
{
	return ( missiles[i].x < 0 ) ||
		( missiles[i].x > G_WIDTH ) ||
		( missiles[i].y < 0 ) ||
		( missiles[i].y > G_HEIGHT );
}

void m_update ( int i )
{
	if ( missiles[i].direction == 1 ){
		missiles[i].x = missiles[i].x + 2;
	}else{
		missiles[i].x = missiles[i].x - 2;
	}
}

void m_draw()
{
	int i=0;

	i = m_first;
	while ( i != m_last ){
		/* if ( m_outside ( i ) ){ */
		/* 	m_first = (m_first + 1) % 10; */
		/* 	mvprintw ( HEIGHT-2, WIDTH-10, */
		/* 		   "%d active", m_last-m_first+1); */
		/* 	continue; */
		/* } */
		m_update ( i );
		if ( missiles[i].potency == 0 ){ /* skip burst missiles */
			i = (i+1) % 10;
			continue;
		}
		mvwprintw ( g_win, missiles[i].y, missiles[i].x, "--" );
		wrefresh ( g_win );
		i = (i+1) % 10;
	}
}

void s_draw(int x, int y, int sign)
{
	mvwprintw ( g_win, y, x, "[]" ); /* API:different ordering */
	if(sign >= 0){
		mvwprintw ( g_win, y, x+2, ">"); 
	}else{
		mvwprintw ( g_win, y, x-1,  "<");
	}
}

void s_process(int c, int *ptry, int *ptrx, int *ptrsign)
{
	switch ( c ){
	case KEY_PPAGE:
		(*ptrx)++;
		(*ptry)--;
		break;
	case KEY_NPAGE:
		(*ptrx)++;
		(*ptry)++;
		break;
	case KEY_HOME:
		(*ptrx)--;
		(*ptry)--;
		break;
	case KEY_END:
		(*ptrx)--;
		(*ptry)++;
		break;
	case KEY_RIGHT:
		(*ptrx)++;
		break;
	case KEY_LEFT:
		(*ptrx)--;
		break;
	case KEY_UP:
		(*ptry)--;
		break;
	case KEY_DOWN:
		(*ptry)++;
		break;
	case 'f' :
		fire ( current_weapon, direction  );
		break;
	case 'A':
	case 'a':
		*ptrsign=(*ptrsign)*-1;
		break;
	case 'q':
	case 'Q':
		status = STATUS_EXIT;
	}
}
/* forbid the player's ship moving out of the game board */
void s_bound ( int *ptrx, int *ptry )
{
	if ( *ptrx < 2 ){
		*ptrx = 2;
		return;
	}
	if ( *ptrx > G_WIDTH-4 ){
		*ptrx = G_WIDTH-4;
		return;
	}
	if ( *ptry < 2 ) {
		*ptry = 2;
		return;
	}
	if ( *ptry > G_HEIGHT-4 ) {
		*ptry = G_HEIGHT-4;
		return;
	}
}
