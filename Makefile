OBJS = sprite.o transport.o main.o
CC = gcc
CFLAGS = -O3
LFLAGS = -lncurses

all:	program

program:	$(OBJS)
	$(CC) -o game.exe $(OBJS) $(LFLAGS)
	strip game.exe
sprite.o:	sprite.c game.h
	gcc -c sprite.c
transport.o:	transport.c
	gcc -c transport.c
main.o:	main.c game.h
	gcc -c main.c
clean:
	rm -f game.exe $(OBJS)
