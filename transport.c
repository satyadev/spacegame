#include "game.h"
#include <ncurses.h>

/* defined here instead of game.h in order to avoid duplication */
char transport[][4]={"[=]",    /* Healthy transport	*/
		     ":-:",    /* injured transport	*/
		     ":::"};    /* exploded transport	*/

void t_spawn()
{
	t_sign = (t_sign==1)?-1:1;
	t_x = (t_sign==1)?0:G_WIDTH;
	t_y = (t_y+2)%G_HEIGHT;
	t_state = HEALTHY;
	t_draw();
}

/* transport is symmetric, hence sign is immaterial */
void t_draw()
{
	if ( t_state == INACTIVE ){
		return;
	}

	/* mvwaddstr ( g_win, t_y, t_x, transport[t_state] ); */

	mvwaddch ( g_win, t_y, t_x, transport[t_state][0] | COLOR_PAIR(3));
	mvwaddch ( g_win, t_y, t_x+1, transport[t_state][1] | COLOR_PAIR(1));
	mvwaddch ( g_win, t_y, t_x+2, transport[t_state][2] | COLOR_PAIR(3));

}

/* main must call t_update(); t_draw(); */
void t_update()
{
	if ( t_state == INACTIVE ) {
		return;
	}
	if ( t_sign == -1 ) {
		t_x --;
	}else{
		t_x ++;
	}
	t_take_damage();
}

void t_take_damage()
{
	int i = m_first;

	if ( t_state == INACTIVE ) {
		return;
	}

	/* check for collision with ship */
	if ( t_x == s_x && t_y == s_y ){
		endwin();
		printf("Game Over\nYou scored %d\n", score);
		exit(0);
	}

	while ( i != m_last ){
		if ( missiles[i].x == t_x && missiles[i].y == t_y
		     && missiles[i].potency > 0 ){
			t_state ++;
			score += 1000;
			missiles[i].potency = 0;
			if ( t_state == INACTIVE ){
				score += 10000;
				mvprintw(HEIGHT-2,WIDTH-20,"Score: %d", score);
				refresh();
				return;
			}
			mvprintw(HEIGHT-2,WIDTH-20,"Score: %d", score);
			refresh();

		}
		i = (i+1) % 10;
	}
}

	

