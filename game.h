#ifndef __GAME_H
#define __GAME_H 1

#include <stdio.h>
#include <ncurses.h>
#include <stdlib.h>


/*
------------------------------------------------
 The transport ship data structures and functions
-------------------------------------------------
*/
enum health {HEALTHY, INJURED, EXPLODED, INACTIVE};
enum health t_state;
int t_x, t_y, t_sign;     /* transport attrs    */

void t_spawn ();
void t_draw ();
void t_update ();
void t_take_damage ();


/*
-------------------------------------------------
 Player ship and missiles: data structures and functions
-------------------------------------------------
*/
enum directions { EAST, WEST };
int s_x, s_y, s_sign;	/* ship co-ordinates */
int m_first; /* index of first missile */
int m_last;  /* index of last missile */
struct missile{
	int x;
	int y;
	int direction;
	int potency;
};
struct missile missiles[10]; /* queue of active missiles */

void m_draw ();
void s_delete ( int x, int y, int sign );
void s_draw ( int x, int y, int sign );
void s_process ( int c, int *ptrx, int *ptry, int *ptrsign );
void s_bound ( int *ptrx, int *ptry );


/*
------------------------------------------------
 Game attributes and functions
------------------------------------------------
*/
int HEIGHT; /* height of game board */
int WIDTH;  /* width of the game board */
int G_HEIGHT;
int G_WIDTH;
WINDOW *g_win; /* active window in the game */


int status; /* maintains overall game status */
int score;

#define STATUS_OK 1
#define STATUS_EXIT -1

#define INTERVAL 1000 /* update interval in milliseconds */

void update ( int signum );


#endif
